#
# rsync files in coupler dir with install locations on remote machines
#
# Larry Solheim Feb,2014

# Determine kernel type and machine name
MACH_TYPE := $(shell uname -s|tr '[A-Z]' '[a-z]')
MACH_NAME := $(word 1,$(subst ., ,$(shell uname -n|awk -F'.' '{print $1}' -)))

ifeq ($(MACH_TYPE), linux)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(shell dnsdomainname))
endif

ifeq ($(MACH_TYPE), aix)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(word 2,$(shell grep -E 'search|domain' /etc/resolv.conf)))
endif

# Determine location, typically either cccma or cmc
LOCATION := $(word 1,$(subst ., ,$(DOMAIN)))
ifndef LOCATION
  $(error Unable to determine a value for LOCATION.)
endif

# INSTALL_SRC names the directory on the remote machine
# into which the files will be installed via rsync
INSTALL_SRC := '/users/tor/acrn/rls/src/coupler'

# A list of files/dirs to be copied to the remote location by the sync target
# Note: the data dir is under version control but is not copied by the sync target
SRCFILES := run bin src

# Define the location of the local git repository containing the coupler
LOCAL_GIT_REPO := /HOME/rls/src/coupler

# This will expand to the full pathname of the git mirror dir on the remote machine
# (either joule or lxwrk1) assuming CCRNSRC is defined in that environment
REM_GIT_REPO := '$$CCRNSRC/coupler_dir/coupler.git'

# The path to the coupler repo on rls001@sci-eccc-in.science.gc.ca
REM_REPO_XC40 := /home/rls001/repo-depo/coupler.git

# This will expand to the full pathname of the bin dir on the remote machine
# Note that this location is different from the bin dir found in INSTALL_SRC
REM_BIN := '$$CCRNSRC/coupler_dir/bin'

# This will expand to the full pathname of the coupler data dir on the remote machine
REM_DATA := '$$CCRNSRC/coupler_dir/coupler_data'

# This help is the default target so that typing just "make" will not do anything destructive
.PHONY: help
help:
	@echo "Goals used to update coupler source code are the following."
	@echo "make sync   ...syncronize local dirs $(SRCFILES) with dirs in joule:$(INSTALL_SRC)"
	@echo "make mirror ...mirror the local coupler repository in $(LOCAL_GIT_REPO) on"
	@echo "                   joule:$(shell ssh joule echo $(REM_GIT_REPO))"
	@echo "                   lxwrk1:$(shell ssh lxwrk1 echo $(REM_GIT_REPO))"

.PHONY: debug
debug:
	@echo DOMAIN = $(DOMAIN)
	@echo LOCATION = $(LOCATION)
	@echo INSTALL_SRC = $(INSTALL_SRC)
	@echo "GIT_PATH_DOR = $(shell ssh joule  echo $(REM_GIT_REPO))"
	@echo "GIT_PATH_VIC = $(shell ssh lxwrk1 echo $(REM_GIT_REPO))"

# sync all files/dirs in SRCFILES with the dir INSTALL_SRC on joule
.PHONY: sync
sync:
ifeq ($(MACH_NAME), lxlp01)
	@# the remote machine will always be joule in this case
	@rsync -CavR --delete-excluded --chmod=Dg+rx,Fg+r,g+X \
	  --exclude '*.lst' --exclude '*.mod' --exclude '*.o' --exclude '*.a' \
	  --exclude '/tmp' --exclude '/evaluate' \
	  --exclude '/scrip' --exclude '/doc' \
	  $(SRCFILES) joule:$(INSTALL_SRC)
else
	$(error rsync is only allowed when originating in Victoria on lxlp01)
endif

# sync the local bin dir with a remote bin dir (this is different than bin in INSTALL_SRC)
.PHONY: bin
bin:
ifeq ($(MACH_NAME), lxlp01)
	rsync -Cav --delete --chmod=Dg+rx,Fg+r,g+X,o+rx bin/ joule:$(REM_BIN)
	rsync -Cav --delete --chmod=Dg+rx,Fg+r,g+X,o+rx bin/ lxwrk1:$(REM_BIN)
else
	$(error bin is only allowed when originating in Victoria on lxlp01)
endif

# Copy these scripts to ~acrnopt/package/bin so they are available to all
.PHONY: cmprs
cmprs: bin/cmprs
	scp bin/cmprs acrnopt@joule:'$$HOME/package/bin/cmprs'
	scp bin/cmprs acrnopt@lxwrk1.cccma.ec.gc.ca:'$$HOME/package/bin/cmprs'
	scp bin/cmprs scrd102@sci-eccc-in.science.gc.ca:'$$HOME/package/bin/cmprs'

.PHONY: gitrip
gitrip: bin/gitrip
	scp bin/gitrip acrnopt@joule:'$$HOME/package/bin/gitrip'
	scp bin/gitrip acrnopt@lxwrk1.cccma.ec.gc.ca:'$$HOME/package/bin/gitrip'
	scp bin/gitrip scrd102@sci-eccc-in.science.gc.ca:'$$HOME/package/bin/gitrip'

.PHONY: amip-bc-cc2nc
amip-bc-cc2nc: bin/amip-bc-cc2nc
	scp bin/amip-bc-cc2nc acrnopt@joule:'$$HOME/package/bin/amip-bc-cc2nc'
	scp bin/amip-bc-cc2nc acrnopt@lxwrk1.cccma.ec.gc.ca:'$$HOME/package/bin/amip-bc-cc2nc'
	scp bin/amip-bc-cc2nc scrd102@sci-eccc-in.science.gc.ca:'$$HOME/package/bin/amip-bc-cc2nc'

# Ensure that there are no uncommitted changes in the local git repo
check_local_git_repo: $(LOCAL_GIT_REPO)
	echo "Checking for changes in $(LOCAL_GIT_REPO)"
	@# The following command returns a non-zero exit status when uncommitted changes exist
	@cd $(LOCAL_GIT_REPO) && git diff-index --exit-code HEAD
	@echo "No changes. $(LOCAL_GIT_REPO) is up to date."

# Make sure that LOCAL_GIT_REPO is a valid directory name
$(LOCAL_GIT_REPO):
	test -d $(LOCAL_GIT_REPO)

# Push to the remote repositories after checking that there are no changes in the local repo
mirror: check_local_git_repo
ifeq ($(MACH_NAME), lxlp01)
	cd $(LOCAL_GIT_REPO) && \
	git push --force --mirror joule.cmc.ec.gc.ca:$(shell ssh joule echo $(REM_GIT_REPO)) && \
	git push --force --mirror lxwrk1.cccma.ec.gc.ca:$(shell ssh lxwrk1 echo $(REM_GIT_REPO)) && \
	git push --force --mirror rls001@sci-eccc-in.science.gc.ca:$(REM_REPO_XC40)
	rsync -Cav --delete --chmod=Da+rx,Fa+r coupler_data/ joule:$(REM_DATA)
	rsync -Cav --delete --chmod=Da+rx,Fa+r coupler_data/ lxwrk1:$(REM_DATA)
else
	$(error The mirror target may only excute on lxlp01)
endif

# Delete then recreate remote mirror dirs as empty repos
# This is necessary when the remote becomes corrupted or gets deleted

# This target will delete and then recreate the remote mirrored dir as an empty repo
# It can then be repopulated using the mirror target below
create_mirror_xc40:
ifeq ($(MACH_NAME), lxlp01)
	ssh rls001@sci-eccc-in.science.gc.ca rm -fr $(REM_REPO_XC40) \&\& git init --shared=0755 --bare $(REM_REPO_XC40)
else
	$(error The create_mirror_xc40 target may only excute on lxlp01)
endif

# This target will delete and then recreate the remote mirrored dir as an empty repo
# It can then be repopulated using the mirror target below
create_mirror_dor:
ifeq ($(MACH_NAME), lxlp01)
	ssh joule rm -fr $(REM_GIT_REPO) \&\& git init --shared=0755 --bare $(REM_GIT_REPO)
else
	$(error The create_mirror_dor target may only excute on lxlp01)
endif

# This target will delete and then recreate the remote mirrored dir as an empty repo
# It can then be repopulated using the mirror target below
create_mirror_vic:
ifeq ($(MACH_NAME), lxlp01)
	ssh lxwrk1 rm -fr $(REM_GIT_REPO) \&\& git init --shared=0755 --bare $(REM_GIT_REPO)
else
	$(error The create_mirror_vic target may only excute on lxlp01)
endif
