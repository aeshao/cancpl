#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
# This file contains CCCma specific macro definitions that may be included
# in a makefile (requires GNU make 3.82 or later)
#
# Larry Solheim  ...Mar,2014
#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
#
# SRC_DIRS, if defined prior to this file being included, must be a whitespace
# separated list of directory names.
# Find all fortran source files in directories specified by SRC_DIRS
# These files will along with all dependent files will be assigned to the variable
# ALL_FSRC by make_defs_cccma.mk and this list will be ordered according to module
# and include dependenies (least dependent first).
# Only the basename of each file will appear in ALL_FSRC.
# make_defs_cccma.mk will also write dependency rules into the current makefile
# for any of the files in FSRC that have module or include dependencies
#
# If FSRC is defined before this file is included then it is assumed to contain the list
# of source files for which dependencies are to be determined and SRC_DIRS is ignored.
# If defined, file names in FSRC must contain any necessary directory components
# (relative or absolute) to be visible from the directory containing this makefile.
# After being processed by make_defs_cccma.mk FSRC will contain only the
# basename part for each file name in the list.
#
# The following variables are defined here
#   MACH_TYPE    ...kernel type on invoking machine
#   MACH_NAME    ...name of invoking machine
#   LOCATION     ...physical location of invoking machine (e.g. cccma, cmc)
#   MODVER       ...the AGCM model version (e.g. gcm16, gcm17, ...)
#   UP2CPP_OPTS  ...command line options to be used with up2cpp
#   FC           ...the name of the fortran compiler (e.g. pgf90, mpxlf90, ...)
#   FFLAGS       ...fortran compiler options to use with FC
#   FIXED        ...FC option used to indicate fixed format fortran source
#   FREE         ...FC option used to indicate free format fortran source
#   LIST_SRC     ...FC option used to produce a source listing at compile time
#   NETCDF_INCS  ...FC options used to locate netcdf includes
#   NETCDF_LIBS  ...FC options used to locate netcdf libraries
#   STAMP        ...a date/time string suitable for appending to file names
#   VPATH        ...initialized with CCCma source directories
#   ADD2VPATH    ...a space separated list of dirs to prepend to VPATH
#
# The following variables may be defined either in the invoking environment
# or on the make command line of the makefile that includes this file.
# These user supplied variables will affect the values of the variables defined above
# All these variables have default values.
#   modver  ...AGCM model version                                      (default gcm18)
#   mpi     ...used to define the the update token "MPI"               (default on)
#   coupled ...used to define the the update token "COUPLED"           (default on)
#   cppdefs ...the name of a file containing cpp preprocessor directives
#              (default is CPP_I if that file exists in cwd, undef otherwise)
#   float1  ...used in the determination of the default compiler       (default off)
#              float1=on is the only value that will have any effect
#   openmp  ...used in the determination of the default compiler       (default off)
#              openmp=on is the only value that will have any effect
#   FC      ...the name of the fortran compiler (e.g. mpxlf90, pgf90, ...)
#              (default depends on other parameters such as float1, openmp, ...)
#   FFLAGS  ...fortran compiler options
#              (default depends on other parameters such as FC, float1, openmp, ...)
#   netcdf  ...used to set compiler options for using netcdf libraries (default on)
#   p5lib   ...used to set compiler options that force the use of a particular set
#              of libraries compatible with an old power5 system       (default off)
#   xlfqinitauto ...used to set compiler options that cause automatic arrays to be
#              initialzed or not initialized                           (default on)
#              Other compiler options are also affected by xlfqinitauto
#   xlfimp  ...used to keep or remove the "-qflttrap=imprecise" option from
#              xlf compiler command line options                       (default on)
#   xlflarge...used to add size load options for the xlf compiler      (default off)
#   libdiag ...used to link against the compiled library libLOSUB_diag.a, or variant
#              in addition to other standard compiled libraries        (default off)
#   ccrnsrc_action ...when ccrnsrc_action=show a list of files from CCRNSRC
#              that are required for the current build will be written to stdout
#              and the program will then stop.
#              When ccrnsrc_action=find then dependent files from CCRNSRC will be
#              found and the build will continue to completion.        (default undef)

# Define a string to append to file names etc
STAMP := $(shell date "+%Y_%b_%d_%H%M%S_$$$$")

# The root of the acrnopt package directory
OPT_ROOT := $(shell perl -e '$$x=(getpwnam "acrnopt")[7]."/package"; print $$x if -d $$x' 2>/dev/null)
ifndef OPT_ROOT
  OPT_ROOT := $(shell perl -e '$$x=(getpwnam "scrd102")[7]."/package"; print $$x if -d $$x' 2>/dev/null)
endif
ifndef OPT_ROOT
  $(warning OPT_ROOT is not defined.)
endif

# Define a string to identify the kernel/hardware type
os=$(shell uname -s 2>/dev/null)
hw=$(shell uname -m 2>/dev/null)
ifeq ($(strip $(os)-$(hw)),Linux-x86_64)
  ARCH := linux64
else ifeq ($(strip $(os)),AIX)
  ARCH := aix64
else
  $(error Unrecognized architecture $(os) $(hw))
endif
undefine os
undefine hw

# Determine kernel type (as lower case) and machine name
MACH_TYPE := $(shell uname -s|tr '[A-Z]' '[a-z]')
MACH_NAME := $(word 1,$(subst ., ,$(shell uname -n|awk -F'.' '{print $1}' -)))

ifeq ($(MACH_TYPE), linux)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(shell dnsdomainname))
endif

ifeq ($(MACH_TYPE), aix)
  # Determine dns domain name
  DOMAIN := $(subst int.cmc.,cmc.,$(word 2,$(shell grep -E 'search|domain' /etc/resolv.conf)))
endif

# Determine location, typically either cccma or cmc
LOCATION := $(word 1,$(subst ., ,$(DOMAIN)))
ifndef LOCATION
  $(error Unable to determine a value for LOCATION.)
endif

# MODVER is the AGCM model version (e.g. gcm16, gcm17, ...)
ifdef modver
  # Intialize with a user supplied value
  # modver is defined either in the invoking environment or on the make command line
  MODVER := $(modver)
endif

ifndef MODVER
  # Set a default value for modver
  MODVER := gcm18
  $(warning modver is not defined. Using modver = $(MODVER))
endif

# Define certain command line options to be used with up2cpp
UP2CPP_OPTS := --overwrite --quiet

# mpi = on means define the update token MPI otherwise MPI is undefined
# This is used in up2cpp where update directives are converted to cpp directives
ifndef mpi
  # mpi defaults to "on"
  mpi := on
  $(warning mpi is not defined. Using mpi = $(mpi))
endif
ifeq ($(mpi), on)
  UP2CPP_OPTS += --mpi
endif
ifeq ($(mpi), ON)
  UP2CPP_OPTS += --mpi
endif

# coupled = on means define the update token COUPLED otherwise COUPLED is undefined
# This is used in up2cpp where update directives are converted to cpp directives
ifndef coupled
  # coupled defaults to "on"
  coupled := on
  $(warning coupled is not defined. Using coupled = $(coupled))
endif
ifeq ($(coupled), on)
  UP2CPP_OPTS += --coupled
endif
ifeq ($(coupled), ON)
  UP2CPP_OPTS += --coupled
endif

# Add an option to up2cpp that will identify a file containing cpp token definitions
ifdef cppdefs
  # If the user has supplied a value for cppdefs on the command line then use it
  # Verify that cppdefs exists
  ifneq ($(cppdefs),$(wildcard $(cppdefs)))
    $(error cppdefs=$(cppdefs) does not exist)
  endif
else
  # Otherwise use CPP_I if a file by that name exists
  cppdefs := $(wildcard CPP_I)
endif

# Note that cppdefs is not necessarily defined at this point
ifdef cppdefs
  UP2CPP_OPTS += cppdefs=$(cppdefs)
endif

# If cppdefs is defined then preprocess the user supplied cppdefs file to remove lines
# that do not begin with "#" and put the resulting file into the current directory.
# These extra line are problematic when free format F90 source code is used and the
# cppdefs file contains fixed format fortran comments or visa versa
# This assumes that all fortran source lines found in cppdefs are comments
ifdef cppdefs
  # Prefix the user supplied name with STAMP to get the local name
  local_cppdefs := $(join $(join $(STAMP),_),$(notdir $(cppdefs)))

  # Make cppdefs into an absolute path name
  override cppdefs := $(abspath $(cppdefs))
  $(info cppdefs = $(cppdefs))
  $(info local_cppdefs = $(local_cppdefs))

  # Use sed to strip non-directives from the cppdefs file
  exit_status_file := tmp_exit_status_sed_cppdefs_$(STAMP)
  $(shell sed -n '/^#/p' $(cppdefs) > $(local_cppdefs); echo $$? > $(exit_status_file))
  exit_status := $(shell cat $(exit_status_file); rm -f $(exit_status_file))
  ifneq ($(strip $(exit_status)),0)
    $(error Problem creating local cppdefs file $(local_cppdefs))
  endif
endif

# netcdf is used to set compiler options for using netcdf libraries
ifndef netcdf
  # netcdf defaults to "on"
  netcdf := on
endif

# p5lib is used to set compiler options that force the use of a particular set
# of libraries compatible with an old power5 system that was used long ago
ifndef p5lib
  # p5lib defaults to "off"
  p5lib := off
endif

# xlfqinitauto is used to set compiler options that cause automatic arrays to be
# initialzed or not initialized
# Other compiler options are also influenced by xlfqinitauto (see below)
ifndef xlfqinitauto
  # xlfqinitauto defaults to "on"
  xlfqinitauto := on
endif

# xlfimp is used to keep or remove the "-qflttrap=imprecise" option
# from the xlf compiler command line options defined in FFLAGS
ifndef xlfimp
  # xlfimp defaults to "on"
  xlfimp := on
endif

# xlflarge is used to keep or remove page size options from
# the xlf compiler command line options defined in FFLAGS
ifndef xlflarge
  # xlflarge defaults to "off"
  xlflarge := off
endif

# libdiag is used to indicate that libLOSUB_diag_... is to be used
ifndef libdiag
  # libdiag defaults to "off"
  libdiag := off
endif

# libsfx will contain a suffix appended to files containing libraries that are
# found in the $(RTEXTBLS) directory if it exists
# libsfx will possibly (likely) be redefined below
libsfx := float2

# Define a default fortran compiler and compiler flags using variable
# definitions found in the invoking environment
ifeq ($(float1), on)
  # float1 is defined and is equal to "on"
  ifeq ($(openmp), on)
    # The user has supplied a value for openmp and that value is "on"
    # Use F77_float1_openmp to determine the default fortran compiler and options
    # if it is defined in the current env
    ifdef F77_float1_openmp
      def_F77 := $(F77_float1_openmp)
    else
      $(warning openmp=on was found in the environment but F77_float1_openmp is not defined)
    endif
    # Redefine libsfx accordingly
    libsfx := float1_openmp
  else
    # Otherwise use F77_float1 to determine the default fortran compiler and options
    # if it is defined in the current env
    ifdef F77_float1
      def_F77 := $(F77_float1)
    else
      $(warning float1=on was found in the environment but F77_float1 is not defined)
    endif
    # Redefine libsfx accordingly
    libsfx := float1
  endif
else
  ifeq ($(openmp), on)
    # The user has supplied a value for openmp and that value is "on"
    # Use F77_openmp if it is defined in the current environment
    ifdef F77_openmp
      def_F77 := $(F77_openmp)
    else
      $(warning openmp=on was found in the environment but F77_openmp is not defined)
    endif
    # Redefine libsfx accordingly
    libsfx := openmp
  else
    # Otherwise use F77_float2 if it is defined in the current env
    ifdef F77_float2
      def_F77 := $(F77_float2)
    endif
    # Redefine libsfx accordingly
    libsfx := float2
  endif
endif

# Define a filter to remove compiler options that determine free or fixed format
fsrc_fmt_filter := s/(-fixed|-free|-Mfixed|-Mfree|-ffixed-form|-ffree-form|-qfixed(=\w+)?|-qfree(=\w+)?)//g

ifdef def_F77
  # Extract a default fortran compiler and compiler flags from the value of def_F77
  def_FC := $(word 1, $(def_F77))
  def_FFLAGS := $(wordlist 2, 1000, $(def_F77))

  # Remove compiler options that determine free or fixed format
  # These options will be added on a per target basis
  def_FFLAGS := $(shell echo $(def_FFLAGS)|perl -pe '$(fsrc_fmt_filter)')
else
  test_value = $(or $(findstring environment,$(origin FC)), \
                    $(findstring command,$(origin FC)), \
                    $(findstring file,$(origin FC)))
  ifeq "$(test_value)" ""
    # If FC was not defined in the env, command line or in a file
    # then this is fatal error
    $(error FC is not set and no default fortran compiler is available.)
  endif
endif

# The previously defined default compiler and compiler options will be used
# unless the user has supplied values for FC and/or FFLAGS
ifdef def_FC
  test_value = $(or $(findstring environment,$(origin FC)), \
                    $(findstring command,$(origin FC)), \
                    $(findstring file,$(origin FC)))
  ifeq "$(test_value)" ""
    # If FC was not defined in the env, command line or in a file then set it here
    FC := $(def_FC)
  endif
endif
ifdef def_FFLAGS
  test_value = $(or $(findstring environment,$(origin FFLAGS)), \
                    $(findstring command,$(origin FFLAGS)), \
                    $(findstring file,$(origin FFLAGS)))
  ifeq "$(test_value)" ""
    # If FFLAGS was not defined in the env, command line or in a file then set it here
    FFLAGS := $(def_FFLAGS)
  endif
endif

# Remove compiler options from FFLAGS that determine free or fixed format
# These options will be added on a per target basis
# Use the override directive here so that FFLAGS will get defined even when it has been
# set on the make command line (command line defs cannot be changed by ordinary assigments)
override FFLAGS := $(shell echo $(FFLAGS)|perl -pe '$(fsrc_fmt_filter)')

ifeq ($(mpi), on)
  ifeq ($(FC:xlf%=xlf), xlf)
    # This is the xlf compiler but does not have the "mp" prefix
    # Add the "mp" prefix to the compiler name
    override FC := $(subst xlf,mpxlf,$(FC))
  endif
endif

# Define a variable that may be used below to identify the type of compiler
ifeq ($(FC:pgf%=pgf), pgf)
  # Assume portland group fortran compiler
  FCTYPE := pgf
endif

ifeq ($(FC:xlf%=xlf), xlf)
  # Assume IBM xlf fortran compiler
  FCTYPE := xlf
endif

ifeq ($(FC:mpxlf%=mpxlf), mpxlf)
  # Assume IBM xlf fortran compiler
  FCTYPE := xlf
endif

ifeq ($(FC:gfo%=gfo), gfo)
  # Assume GNU fortran compiler
  FCTYPE := gnu
endif

ifeq ($(FC:ftn%=ftn), ftn)
  # Assume INTEL fortran compiler
  FCTYPE := ftn
endif

ifeq ($(strip $(FCTYPE)),xlf)
  # IBM xlf fortran compiler
  # Filter command line options depending on the value of certain variables
  # namely xlfqinitauto, xlfimp and xlflarge

  # Define a BEGIN block containing a perl procedure to remove a word beginning with a
  # user supplied prefix from a ":" separated list of words also supplied by the user
  # This is used in the perl filters that are run below
  begin := BEGIN {$$strip_opt = sub{$$pfx=shift; $$str=shift; \
                  $$str =~ s/(:$$pfx\w*|$$pfx\w*:)//ig; \
                  if ($$str =~ s/($$pfx\w*)//ig) {return undef} else {return $$str}}}

  ifeq ($(strip $(xlfqinitauto)),off)
    # Append _noxlfqinitauto to libsfx
    libsfx := $(join $(libsfx),_noxlfqinitauto)

    # Remove the -qinitauto=... option from the compler command line, if any
    # and the -qfloat=nans option from the compler command line, if any
    # and the -qflttrap=nanq option from the compler command line, if any
    pscript := $(begin) s/-qinitauto(=\w+)?//g; \
                        s/(-qfloat=[:\w]+)/&$$strip_opt("nans",$$1)/eg; \
                        s/(-qflttrap=[:\w]+)/&$$strip_opt("nanq",$$1)/eg;
    override FFLAGS := $(shell echo $(FFLAGS)|perl -pe '$(pscript)')
  endif

  ifeq ($(strip $(xlfimp)),off)
    # Remove the -qflttrap=imprecise option from the compler command line, if any
    pscript := $(begin) s/(-qflttrap=[:\w]+)/&$$strip_opt("imp",$$1)/eg;
    override FFLAGS := $(shell echo $(FFLAGS)|perl -pe '$(pscript)')
  endif

  ifeq ($(strip $(xlflarge)),on)
    # Add some ld options to the xlf command line that will set page size to 64K, namely
    #   -bdatapsize:64K -bstackpsize:64K -btextpsize:64K
    # First remove any existing page size options and then append them to the end of FFLAGS
    pscript := s/(-bdatapsize:64K|-bstackpsize:64K|-btextpsize:64K)//g;
    override FFLAGS := $(shell echo $(FFLAGS)|perl -pe '$(pscript)')
    override FFLAGS := $(FFLAGS) -bdatapsize:64K -bstackpsize:64K -btextpsize:64K
  endif
endif

# kernel specific definitions

# Netcdf location and fortran link options
ifneq ($(strip $(FCTYPE)),ftn)
  ifdef OPT_ROOT
    # The root of the netcdf install directory
    NETCDF_ROOT := $(OPT_ROOT)/netcdf/$(ARCH)
    # Fortran compiler link options for netcdf
    NETCDF_INCS := $(shell $(OPT_ROOT)/bin/nf-config --xopt-fc=$(FC) --fflags)
    NETCDF_LIBS := $(shell $(OPT_ROOT)/bin/nf-config --xopt-fc=$(FC) --flibs)
  else
    $(warning NETCDF_INCS and NETCDF_LIBS are not defined.)
  endif
endif

ifeq ($(MACH_TYPE), linux)
  CPP := /usr/bin/cpp
  CPPFLAGS += -P

  CC := gcc
  CFLAGS += -g
endif

ifeq ($(MACH_TYPE), aix)
  CPP := /usr/bin/cpp
  CPPFLAGS += -P

  CC := xlc_r
  CFLAGS += -g
endif

$(info CPPFLAGS = $(CPPFLAGS))

# Define compiler command line options for libraries and includes, if any
# Compiled libraries exist in the dir pointed to by the env variable RTEXTBLS
# Library names are of the form:
#     libLOSUB_model_${modver}_${sfx}.a
#     libLOSUB_comm_${sfx}.a
#     libLOSUB_diag_${sfx}.a
#     libLOSUB_plots_${sfx}.a
# the sfx may be absent and if it is then the preceeding underscore is as well
# sfx is usually one of:
#     float1
#     float1_openmp
#     float1_openmp_noxlfqinitauto
#     float2
#     openmp_noxlfqinitauto
# Linking against the libLOSUB_model_... and libLOSUB_comm_... libraries is
# all that is required to compile the agcm driver (e.g. gcm16.dk, gcm17.dk, ...)

ifeq ($(strip $(INCS)),)
  # Define INCS only if it is not yet defined
  # Always include the current directory
  INCS := -I.

  ifdef ADD2VPATH
    # Add each dir in ADD2VPATH
    INCS += $(foreach dir,$(ADD2VPATH),-I$(dir))
  endif

  ifeq ($(strip $(netcdf)),on)
    # Add netcdf related include directories
    INCS += $(NETCDF_INCS)
  endif

endif

# $(info 0 LIBS = $(LIBS)   origin = $(origin LIBS))

ifeq ($(strip $(LIBS)),)
  # Define LIBS only if it is not yet defined
  # def_LIBS_here is required because LIBS is defined then appended to further down
  # Therefore LIBS will already be defined when appended to and testing for LIBS to
  # be empty prior to appending will not work
  def_LIBS_here := yes
endif

# If FC is defined then LIBS will need to be assigned
# using override in certain locations below
override_LIBS = $(or $(findstring environment,$(origin FC)), \
                     $(findstring command,$(origin FC)))

ifeq ($(strip $(def_LIBS_here)),yes)
  # Define LIBS only if it is not yet defined
  ifdef RTEXTBLS
    # If set in the current environment RTEXTBLS is the name of a directory
    # containing a number of precompiled libraries with names of the form
    #   libLOSUB_model_$(MODVER)$(libsfx).a
    #   libLOSUB_model_diag$(libsfx).a
    #   libLOSUB_model_comm$(libsfx).a
    ifneq ($(strip $(libsfx)),)
      libsfx := $(join _,$(libsfx))
    endif
    ifeq ($(strip $(libdiag)),on)
      diag_LIBS := -lLOSUB_diag$(libsfx)
    endif
    curr_LIBS := -L$(RTEXTBLS) -lLOSUB_model_$(MODVER)$(libsfx) -lLOSUB_comm$(libsfx) $(diag_LIBS)
    ifdef override_LIBS
      override LIBS := $(curr_LIBS)
    else
      LIBS := $(curr_LIBS)
    endif
  endif

  ifeq ($(strip $(netcdf)),on)
    # Add netcdf related libraries
    ifdef override_LIBS
      override LIBS := $(NETCDF_LIBS) $(LIBS)
    else
      LIBS := $(NETCDF_LIBS) $(LIBS)
    endif
  endif

endif

# FCCPPDEFS will contain any cpp macro definitions that should appear
# as fortran compiler options
FCCPPDEFS :=

# Compiler specific definitions

ifeq ($(strip $(FCTYPE)),pgf)
  # Portland group fortran compiler
  FIXED := -Mfixed
  FREE  := -Mfree
  LIST_SRC := -Mlist

  # TODO set this properly for pgf
  ifdef COUPLER_COMMIT_ID
    FCCPPDEFS += -WF,-DCOUPLER_COMMIT_ID=$(COUPLER_COMMIT_ID)
  endif
  ifdef COUPLER_REPO_PATH
    FCCPPDEFS += -WF,-DCOUPLER_REPO_PATH=$(COUPLER_REPO_PATH)
  endif
endif

ifeq ($(strip $(FCTYPE)),xlf)
  # IBM xlf fortran compiler
  FIXED := -qfixed=72
  FREE  := -qfree=f90
  LIST_SRC := -qsource
  ifdef COUPLER_COMMIT_ID
    FCCPPDEFS += -WF,-DCOUPLER_COMMIT_ID=$(COUPLER_COMMIT_ID)
  endif
  ifdef COUPLER_REPO_PATH
    FCCPPDEFS += -WF,-DCOUPLER_REPO_PATH=$(COUPLER_REPO_PATH)
  endif

  ifeq ($(strip $(def_LIBS_here)),yes)
    # Add system libraries to LIBS
    ifeq ($(strip $(p5lib)),on)
      # Include the power5 libraries
      curr_LIBS := $(LIBS) -L/home/crb_ccrn/pollux/acrn/src/plib/p5lib -lessl -lmassvp5 -lmass -lblas
      ifdef override_LIBS
        override LIBS := $(curr_LIBS)
      else
        LIBS := $(curr_LIBS)
      endif
    else
      # Use only current system libraries
      curr_LIBS := $(LIBS) -L/usr/lib -lessl -lmassvp5 -lmass -lblas
      ifdef override_LIBS
        override LIBS := $(curr_LIBS)
      else
        LIBS := $(curr_LIBS)
      endif
    endif
  endif
endif

ifeq ($(strip $(FCTYPE)),gnu)
  # GNU fortran compiler
  FIXED := -ffixed-form
  FREE  := -ffree-form
  #??? LIST_SRC := 

  # TODO set this properly for gfortran
  ifdef COUPLER_COMMIT_ID
    FCCPPDEFS += -WF,-DCOUPLER_COMMIT_ID=$(COUPLER_COMMIT_ID)
  endif
  ifdef COUPLER_REPO_PATH
    FCCPPDEFS += -WF,-DCOUPLER_REPO_PATH=$(COUPLER_REPO_PATH)
  endif
endif

ifeq ($(strip $(FCTYPE)),ftn)
  # INTEL fortran compiler
  FIXED := -fixed
  FREE  := -free
  LIST_SRC := -list

  ifdef COUPLER_COMMIT_ID
    FCCPPDEFS += -DCOUPLER_COMMIT_ID=$(COUPLER_COMMIT_ID)
  endif
  ifdef COUPLER_REPO_PATH
    FCCPPDEFS += -DCOUPLER_REPO_PATH=$(COUPLER_REPO_PATH)
  endif
endif

ifeq ($(strip $(LIBS)),system)
  # If the user has supplied the special value of "system" then ensure that
  # LIBS contains only system libraries and netcdf libs (if any)
  ifeq ($(strip $(netcdf)),on)
    # Use netcdf related libraries if requested
    curr_NCLIBS := $(NETCDF_LIBS)
  endif
  ifeq ($(strip $(FCTYPE)),xlf)
    # This is only valid for xlf compilers
    ifeq ($(strip $(p5lib)),on)
      # Include the power5 libraries
      curr_LIBS := $(curr_NCLIBS) -L/home/crb_ccrn/pollux/acrn/src/plib/p5lib -lessl -lmassvp5 -lmass -lblas
      ifdef override_LIBS
        override LIBS := $(curr_LIBS)
      else
        LIBS := $(curr_LIBS)
      endif
    else
      # Use only current system libraries
      curr_LIBS := $(curr_NCLIBS) -L/usr/lib -lessl -lmassvp5 -lmass -lblas
      ifdef override_LIBS
        override LIBS := $(curr_LIBS)
      else
        LIBS := $(curr_LIBS)
      endif
    endif
  else
    # If not the xlf compiler then undef LIBS except for any netcdf libraries
    ifdef override_LIBS
      override LIBS := $(curr_NCLIBS)
    else
      LIBS := $(curr_NCLIBS)
    endif
  endif
endif

ifeq ($(strip $(LIBS)),none)
  # If the user has supplied the special value of "none" then ensure LIBS is empty
  override LIBS :=
endif

# Append an option to FFLAGS that will produce a source listing at compile time
# and also add any cpp macro definitions that are required
override FFLAGS := $(FFLAGS) $(LIST_SRC) $(FCCPPDEFS)

# Define ESMFMKFILE containing the location of the ESMF include file
# and include this file if it exists
ifdef OPT_ROOT
  # The location of the ESMF application makefile fragment
  ifeq ($(FC:mpxlf%=mpxlf), mpxlf)
    # We require the MPI version of the ESMF include file on hadar or spica
    # Hard code this location for now
    ESMFMKFILE := $(OPT_ROOT)/esmf/aix64-hadar-esmf-6.3.0rp1-xlf-mpi/lib/esmf.mk
  else
    # Currently, all other versions are built without MPI
    ESMFMKFILE := $(OPT_ROOT)/esmf/$(ARCH)/lib/esmf.mk
  endif

  # Include ESMFMKFILE here. This will define
  # ESMF_F90COMPILER
  # ESMF_F90LINKER
  # ESMF_F90COMPILEOPTS
  # ESMF_F90COMPILEPATHS
  # ESMF_F90COMPILECPPFLAGS
  # ESMF_F90COMPILEFREECPP
  # ESMF_F90COMPILEFREENOCPP
  # ESMF_F90COMPILEFIXCPP
  # ESMF_F90COMPILEFIXNOCPP
  # ESMF_F90LINKOPTS
  # ESMF_F90LINKPATHS
  # ESMF_F90LINKRPATHS
  # ESMF_F90LINKLIBS
  # ESMF_F90ESMFLINKLIBS
  # among other ESMF related variables
  ifeq ($(strip $(wildcard $(ESMFMKFILE))),)
    # It is an error if ESMFMKFILE does not exist
    $(error $(ESMFMKFILE) is missing)
  endif
  include $(ESMFMKFILE)
  $(info Using ESMF)

  # Add ESMF compile path options to FFLAGS
  override FFLAGS := $(FFLAGS) $(ESMF_F90COMPILEPATHS)
else
  $(warning ESMF is not available)
endif

ifeq ($(MAKECMDGOALS),debug)
  $(info FC       = $(FC))
  $(info FFLAGS   = $(FFLAGS))
  $(info FIXED    = $(FIXED))
  $(info FREE     = $(FREE))
  $(info LIST_SRC = $(LIST_SRC))
  $(info LIBS     = $(LIBS))
  $(info INCS     = $(INCS))
  $(info NETCDF_INCS = $(NETCDF_INCS))
  $(info NETCDF_LIBS = $(NETCDF_LIBS))
endif

# Define a template used to write dependeny rules at runtime
define DEP_template
  $(strip $(subst ?, ,$(1)))
endef

# Define a code snippet that will expand DEP_template using the parameter $(rule)
# If this is defined using the "info" function then the resulting rule is written to stdout
# If this is defined using the "eval" function then the resulting rule is written to the makefile
write_rule_dbg =  $(if $(rule), $(info $(call DEP_template,$(rule))))
write_rule = $(if $(rule), $(eval $(call DEP_template,$(rule))))

# Define a snippet of GNUmake script that will find all files containing module definitions
# in a directory named $(dir). This is intended to be used with the foreach function.
# A plain "=" must be used to define find_modules as a recursively expanding variable
# that may then be used as the "text" argument in the foreach function
# The "if" condition is required to avoid supplying an empty list to grep,
# which will then wait for stdin and cause the make to appear to be hung
find_modules = $(if $(wildcard $(dir)/*.f $(dir)/*.F $(dir)/*.f90 $(dir)/*.F90 $(dir)/*.dk), \
                 $(shell grep -li '^[ \t]*module[ \t]' \
                   $(wildcard $(dir)/*.f $(dir)/*.F $(dir)/*.f90 $(dir)/*.F90 $(dir)/*.dk)))

# Define a perl script that will read a list of fortran source file names, each containing
# module definitions, from the command line and return the same list of files sorted
# by module dependencies (least dependent first).
# The returned list will only contain the basename of each input file.
# If the variable "deps_file" is exported from the invoking environment then a file named $deps_file
# will be created containing "target: dependencies" lines, one for each file that has dependencies.
# If the variable "dep_rules" is defined true (according to perl) then additional words of the
# form "target:?dep1?dep2...?" will be appended to any output source file list.
# These extra words may be used by the invoking makefile to dynamically generate targets
# for module object files that depend on other module object files
module_deps := BEGIN { use File::Basename; \
                       @list_in = map(basename($$_),@ARGV); \
                       $$deps_file = $$ENV{deps_file}; $$dep_rules = $$ENV{dep_rules}; } \
  if (/^\s*module\s/i) { \
    ($$name) = /^\s*module\s+(.*)$$/i; $$name =~ s/!.*$$//; $$name =~ s/\s*//g; $$name = lc($$name); \
    $$defines{$$name} = basename($$ARGV); \
  } elsif (/^\s*use\s/i) { \
    ($$name) = /^\s*use\s+(.*)$$/i; \
    ($$name) = split /\s*,\s*/, $$name; $$name =~ s/!.*$$//; $$name =~ s/\s*//g; $$name = lc($$name); \
    $$fname = basename($$ARGV); \
    push @{$$uses{$$fname}}, $$name unless grep /^$$name$$/, @{$$uses{$$fname}}; \
  } elsif (/^[\#]?\s*include\s/i) { \
    ($$incfile) = /^[\#]?\s*include\s(.*?)(\s|\/|$$)/i; $$incfile =~ s/!.*$$//; $$incfile =~ s/\s*//; \
    $$incfile =~ s/^\s*\x{27}(.*?)\x{27}\s*$$/$$1/; $$incfile =~ s/^\s*\x{22}(.*?)\x{22}\s*$$/$$1/; \
    $$incfile =~ s/^\s*<(.*?)>\s*$$/$$1/; $$incfile =~ s/\/$$//; next if $$incfile eq "mpif.h"; \
    next if $$incfile eq "netcdf.inc"; $$fname = basename($$ARGV); \
    push @{$$includes{$$fname}}, $$incfile unless grep /^$$incfile$$/, @{$$includes{$$fname}}; \
  } elsif (/^\%\s*call\s/i) { \
    ($$incfile) = /^\%\s*call\s(.*)$$/i; $$incfile =~ s/!.*$$//; $$incfile =~ s/\s*//; \
    next unless $$incfile; $$incfile = lc($$incfile).q{.cdk}; $$fname = basename($$ARGV); \
    push @{$$includes{$$fname}}, $$incfile unless grep /^$$incfile$$/, @{$$includes{$$fname}}; \
  } \
  END { \
    foreach my $$fname (keys %uses) { \
      undef @tlist; push @tlist, map($$defines{$$_}, @{$$uses{$$fname}}); \
      foreach my $$dep (@tlist) { \
        next unless $$dep; next if $$dep eq $$fname; \
        push @{$$depends{$$fname}}, $$dep unless grep /^$$dep$$/, @{$$depends{$$fname}} \
      } \
    } \
    foreach my $$fname (keys %includes) { \
      foreach my $$inc (@{$$includes{$$fname}}) { \
        next unless $$inc; next if $$inc eq $$fname; $$isinc{$$inc} = 1; \
        push @{$$depends{$$fname}}, $$inc unless grep /^$$inc$$/, @{$$depends{$$fname}} \
      } \
    } \
    foreach my $$fname (keys %depends) {push @{$$alldeps{$$fname}},@{$$depends{$$fname}}}; \
    for (1 .. 3) { \
      my $$found = 1; \
      foreach my $$fname (keys %alldeps) { \
        $$count_in = scalar(@{$$alldeps{$$fname}}); \
        foreach my $$dep (@{$$alldeps{$$fname}}) { \
          next unless $$dep; \
          foreach my $$f2 (@{$$depends{$$dep}}) { \
            next if $$f2 eq $$fname; \
            unless (grep /^$$f2$$/,@{$$alldeps{$$fname}}) {push @{$$alldeps{$$fname}},$$f2} \
          } \
        } \
        $$found = 0 unless $$count_in == scalar(@{$$alldeps{$$fname}}); \
      } \
      last if $$found; \
    } \
    sub sort_by_dep { \
      undef @with_deps; undef @no_deps; undef @sorted; \
      foreach (@_) {if ($$depends{$$_}) {push @with_deps,$$_} else {push @no_deps,$$_}} \
      @sorted = sort { \
        $$adep=0; foreach (@{$$alldeps{$$a}}) {if ($$_ eq $$b) {$$adep=1; last}} \
        $$bdep=0; foreach (@{$$alldeps{$$b}}) {if ($$_ eq $$a) {$$bdep=1; last}} \
        $$adep <=> $$bdep; \
      } @with_deps; \
      return @no_deps,@sorted; \
    } \
    print join(" ",sort_by_dep(@list_in)); \
    if ($$deps_file or $$dep_rules) { \
      if ($$deps_file) {open(DEPS_FILE, ">$$deps_file") or die "$!\n"}; \
      foreach (sort keys %depends) { \
        next unless scalar(@{$$depends{$$_}}); \
        my $$target = $$_; $$target =~ s/\..*?$$/.o/ unless $$isinc{$$target}; \
        my @deps = sort_by_dep(@{$$depends{$$_}}); \
        foreach my $$dep (@deps) {$$dep =~ s/\..*?$$/.o/ unless $$isinc{$$dep}} \
        if ($$deps_file) {print DEPS_FILE "$${target}: ",join(" ",@deps),"\n"} \
        if ($$dep_rules) {print " $${target}:?",join("?",@deps),"? "} \
      } \
      foreach (sort keys %isinc) {next if /^xit2.cdk$$/; push @inclist,$$_}; \
      if ($$deps_file) {print DEPS_FILE join(" ",@inclist),":\n" if scalar(@inclist)} \
      if ($$dep_rules) {print " ",join("?",@inclist),":? " if scalar(@inclist)} \
      if ($$deps_file) {close DEPS_FILE}; \
    } \
  }

# Define a CCCma specific set of default rules
# A special rule for xit2 is required since it is identified as a comdeck via the .cdk
# extension but is a complete subroutine unlike other comdecks, which are code snippets
xit2.o: xit2.cdk
	up2cpp $(UP2CPP_OPTS) modver=$(MODVER) --out_file=xit2.F xit2.cdk
	$(FC) -c $(FIXED) $(FFLAGS) xit2.F $(INCS) -o xit2.o

# Files with a .dk extension are lsmod decks that will, in general contain update directives
# These files are preprocessed to convert update directives to cpp directives
# This may mean that a user supplied file containing cpp token definitions is required
# The user can identify a cppdefs file by supplying "cppdefs=FNAME" on the make command line
%.o: %.dk
	up2cpp $(UP2CPP_OPTS) modver=$(MODVER) --out_file=$*.F $*.dk
	$(FC) -c $(FIXED) $(FFLAGS) $*.F $(INCS) -o $*.o

%.h: %.cdk
	up2cpp $(UP2CPP_OPTS) modver=$(MODVER) --out_file=$*.h $*.cdk

%.o: %.F90
ifdef local_cppdefs
	@# Prepend each file with a directive to include cppdefs
	tmpfile=$*_$(STAMP).F90; rm -f $$tmpfile; \
	echo '#include "$(local_cppdefs)"' | cat - $*.F90 > $$tmpfile && \
	$(FC) -c $(FREE) $(FFLAGS) $$tmpfile $(INCS) -o $*.o && rm -f $$tmpfile
else
	$(FC) -c $(FREE) $(FFLAGS) $< $(INCS) -o $*.o
endif

%.o: %.F95
ifdef local_cppdefs
	@# Prepend each file with a directive to include cppdefs
	tmpfile=$*_$(STAMP).F90; rm -f $$tmpfile; \
	echo '#include "$(local_cppdefs)"' | cat - $*.F90 > $$tmpfile && \
	$(FC) -c $(FREE) $(FFLAGS) $$tmpfile $(INCS) -o $*.o && rm -f $$tmpfile
else
	$(FC) -c $(FREE) $(FFLAGS) $< $(INCS) -o $*.o
endif

%.o: %.F
ifdef local_cppdefs
	@# Prepend each file with a directive to include cppdefs
	tmpfile=$*_$(STAMP).F; rm -f $$tmpfile; \
	echo '#include "$(local_cppdefs)"' | cat - $*.F > $$tmpfile && \
	$(FC) -c $(FIXED) $(FFLAGS) $$tmpfile $(INCS) -o $*.o && rm -f $$tmpfile
else
	$(FC) -c $(FIXED) $(FFLAGS) $< $(INCS) -o $*.o
endif

%.o: %.f90
	$(FC) -c $(FREE) $(FFLAGS) $< $(INCS) -o $*.o

%.o: %.f95
	$(FC) -c $(FREE) $(FFLAGS) $< $(INCS) -o $*.o

%.o: %.f
	$(FC) -c $(FIXED) $(FFLAGS) $< $(INCS) -o $*.o

# Cancel the implicit RCS extraction rule to avoid getting out of date files
# or unwanted source files copied to the build directory
% :: RCS/%,v

# If the user has defined ADD2VPATH then prepend this to the
# list of directories that are searched for prerequisits
# ADD2VPATH is a colon or space separated list of dirs
ifdef ADD2VPATH
  vpath % $(ADD2VPATH)
  $(info Added $(ADD2VPATH) to vpath.)
endif

# Where to find lsmod "decks"
vpath %.dk  $(CCRNSRC)/source/lsmod/agcm/$(MODVER)
vpath %.cdk $(CCRNSRC)/source/lsmod/agcm/$(MODVER)

# Where to find other CCCma source files
# VPATH may be appended to or overwritten by the parent makefile
VPATH := $(CCRNSRC)/source/lssub/comm/io \
         $(CCRNSRC)/source/lssub/comm/gen \
         $(CCRNSRC)/source/lssub/comm/nrecipes \
         $(CCRNSRC)/source/lssub/comm/trans \
         $(CCRNSRC)/source/lssub/comm/ocean \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/phys \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/ocean \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/io \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/conv \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/ctem \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/mixed_layer_model \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/modl \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/isccp_simulator \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/rad \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/radforce \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/relax \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/river_routing \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/species \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/trans \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/pam \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/mam_rad \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/mam_nonoro_gwd \
         $(CCRNSRC)/source/lssub/model/agcm/$(MODVER)/class_v36 \
         $(CCRNSRC)/source/lssub/diag/gen \
         $(CCRNSRC)/source/lssub/diag/trans

ifneq "$(strip $(FSRC))" ""
  # NOTCCC_VPATH is a list of all dirs on VPATH that are not part of CCRNSRC
  NOTCCC_VPATH := $(filter-out ./,$(sort $(dir $(FSRC))))
  # Prepend all dirs associated with files in FSRC to VPATH (except ".")
  VPATH := $(NOTCCC_VPATH) $(VPATH)
else
  ifneq "$(strip $(SRC_DIRS))" ""
    # NOTCCC_VPATH is a list of all dirs on VPATH that are not part of CCRNSRC
    NOTCCC_VPATH := $(filter-out .,$(SRC_DIRS))
    # Prepend any user supplied source dirs to VPATH (except ".")
    VPATH := $(NOTCCC_VPATH) $(VPATH)
  endif
endif

$(info VPATH = $(VPATH))

# Assign a variable containing common cccmf command line options
cccmf_opts := modver=$(MODVER)

ifdef cppdefs
  cccmf_opts += cppdefs=$(cppdefs)
endif

ifneq "$(strip $(NOTCCC_VPATH))" ""
  cccmf_opts += path="$(NOTCCC_VPATH)"
endif

ifdef ADD2VPATH
  cccmf_opts += path="$(ADD2VPATH)"
endif

# Define a variable containing a single space to be used in the function subst below
empty :=
space := $(empty) $(empty)

# Add "_dep" to any file in cwd or on VPATH and use this on the make command line
# to create a file containing a list of dependencies for that file
%_dep: %
	@echo "Creating dependency file for $<"
	cccmf $(cccmf_opts) depends_file=deps_for_$< $<

# find_src is used to get a list of all source files to be compiled in a given directory
# $(dir) can be any valid relative or absolute path name
find_src = $(wildcard $(dir)/*.f $(dir)/*.F $(dir)/*.f90 $(dir)/*.F90 $(dir)/*.dk $(dir)/*.cdk)

ifeq "$(strip $(FSRC))" ""
  # Use FSRC if it is defined by the parent makefile
  # otherwise set it to all files found in SRC_DIRS
  ifneq "$(strip $(SRC_DIRS))" ""
    FSRC := $(foreach dir, $(SRC_DIRS), $(find_src))
  endif
endif

# Make a copy of FSRC before it is sorted and before any leading path information
# has been removed from component file names
RAW_FSRC := $(FSRC)

ifeq ($(MAKECMDGOALS),write_depends)
  # Create a file that could be included in an external make file (or simply viewed) then stop
  # This will be suitable for building all user supplied source files
  DEPFILE := depends_$(STAMP)
  exit_status_file := tmp_exit_status_$(STAMP)
  shcmd := cccmf $(cccmf_opts) depends_file=$(DEPFILE) $(RAW_FSRC)
  shcmdout := $(shell $(shcmd); echo $$? > $(exit_status_file))
  exit_status := $(shell cat $(exit_status_file); rm -f $(exit_status_file))
  ifneq ($(strip $(exit_status)),0)
    $(info $(shcmdout))
    $(error Problem executing --> $(shcmd) <--)
  endif
  $(info $(shcmdout))
  $(error Created dependency file $(DEPFILE))
endif

# Define a list of source file names that include all user supplied files as well
# as any source files that contain dependencies for these user supplied files.
# This is done using the external program cccmf. In order for cccmf to find all
# dependencies the user may need to supply additional directory pathnames via ADD2VPATH.
# cccmf will return just basenames (any leading path components are removed) and these
# files will be sorted by dependency with least dependent file first in the list.
exit_status_file := tmp_exit_status_$(STAMP)
shcmd := cccmf --list_only $(cccmf_opts) $(RAW_FSRC)
ALL_FSRC := $(shell $(shcmd) 2>/dev/null; echo $$? > $(exit_status_file))
exit_status := $(shell cat $(exit_status_file); rm -f $(exit_status_file))
ifneq ($(strip $(exit_status)),0)
  $(info $(ALL_FSRC))
  $(error Problem executing --> $(shcmd) <--)
endif

# Extract .cdk files (except xit2.cdk) from ALL_FSRC then prepend the same file names
# to ALL_SRC with each .cdk extension replaced by a .h extension
# This will allow the %.h:%.cdk target written above to replace any update directives
# from theses include files and create the .h files
# The resulting .h include files are required when .dk files are subsequently
# processed by the make file via the %.o:%.dk target written above
HINC_SRC := $(patsubst %.cdk,%.h,$(filter-out xit2.cdk,$(filter %.cdk,$(ALL_FSRC))))
ALL_FSRC := $(HINC_SRC) $(filter-out $(HINC_SRC),$(ALL_FSRC))

ifeq ($(MAKECMDGOALS),list_depends)
  # Simply list all dependent file to stdout then exit
  $(info $(ALL_FSRC))
  $(error Done listing dependent source files)
endif

# $(info ALL_FSRC = $(ALL_FSRC))

ifneq "$(strip $(FSRC))" ""
  # Sort the files according to dependency on modules and included files and return the sorted
  # list along with words of the form "target:?dep1?dep2?" where white space has been replaced
  # with "?" characters and a trailing "?" character has been appended
  DEPINFO := $(shell env dep_rules=1 perl -ne '$(module_deps)' $(FSRC))

  # Extract the sorted files in one list and any "target:?dep1?dep2?" rules in another
  FSRC  := $(filter-out %?, $(DEPINFO))
  RULES := $(filter %?, $(DEPINFO))

  # Write dependency rules into the current makefile
  # This should be done after at least one target has been defined in the makefile
  # If there is no rule above this in the makefile then the first rule created
  # by this procedure will be the default rule (normaly this is not desireable)
  $(foreach rule, $(RULES), $(write_rule))
  ifeq ($(MAKECMDGOALS),debug)
    # Use the "write_rule_dbg" version to also write rules to stdout
    $(foreach rule, $(RULES), $(write_rule_dbg))
  endif
endif

ifdef ccrnsrc_action
  # Get a list of all CCRNSRC files that are required for the current build
  # and assign this list to the variable CCCFSRC
  ifneq ($(strip $(RAW_FSRC)),)
    # The program cccmf is used to determine dependencies
    exit_status_file := tmp_exit_status_$(STAMP)
    shcmd := cccmf $(cccmf_opts) --depends_ccrnsrc $(RAW_FSRC)
    $(info $(shcmd))
    CCCFSRC := $(shell $(shcmd); echo $$? > $(exit_status_file))
    exit_status := $(shell cat $(exit_status_file); rm -f $(exit_status_file))
    ifneq ($(strip $(exit_status)),0)
      $(info $(CCCFSRC))
      $(error Problem executing --> $(shcmd) <--)
    endif
    ifeq ($(strip $(ccrnsrc_action)),show)
      # If the action is "show" then write CCCFSRC to stdout and stop
      $(info Add/replace the following definition in your makefile to include dependent CCRNSRC files.)
      $(info CCCFSRC := $(CCCFSRC))
      $(error Make stops here.)
    else
      # Otherwise print the value of CCCFSRC and continue with the build
      $(info CCCFSRC = $(CCCFSRC))
    endif
  else
    $(info No source files were found. Cannot determine CCRNSRC dependencies.)
  endif
endif
