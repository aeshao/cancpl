#! /usr/bin/env python

import sys
# Add a path for the netcdf4 module
sys.path.append('/HOME/rls/anaconda2/lib/python2.7/site-packages/netCDF4')
import os
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.backends.backend_pdf import PdfPages
import re

verbose = 0

# If batch_mode = T then do not display anything on the users terminal
batch_mode = False

# At first glance argparse has no way to deal with non-option (e.g. file name, val=var)
# command line args. Presumably one could filter sys.argv first but if that was done
# then argparse would be redundant.
#
# import argparse
# parser = argparse.ArgumentParser(version='1.0')
# parser.add_argument('-f', action='store', dest='input_file',
#                     help='Input netcdf file name')
#
# The input netcdf file name
# nc_file_name = parser.parse_args().input_file
# if nc_file_name == None:
#     print "An input file name is required."
#     exit(1)

###########################################################################
# Read the data
###########################################################################

# Read the input netcdf file name from the command line
if len(sys.argv) < 2:
    print "An input file name is required on the command line."
    exit(1)

if os.path.isfile(sys.argv[1]):
    # The first command line arg is the input netcdf file name
    nc_file_name = sys.argv[1]
else:
    print sys.argv[1],"is not a regular file."
    exit(1)

if len(sys.argv) > 2:
    if sys.argv[2] == "batch":
        batch_mode = True

# Identify the netcdf file name
# nc_file_name = 'regrid_test_3x3_to_offset5x5.nc'
# nc_file_name = 'regrid_test_offset5x5_to_3x3.nc'
# nc_file_name = 'regrid_test_6x6_to_3x3.nc'
# nc_file_name = 'regrid_test_3x3_to_6x6.nc'
print "Reading",nc_file_name

# Create a Dataset object containing information about the netcdf file
fh = Dataset(nc_file_name, mode='r')

# Print info about the dataset
# print(fh)

# Print all variable names
# print(fh.variables.keys())

# Print all dimensions
# for d in fh.dimensions.items():
#    print d

# Read global attributes
# for attr in fh.ncattrs():
#     print attr, '=', getattr(fh, attr)
# 
# print "test function ",fh.test_function

# The unlimited dimension is named member
member_max = fh.dimensions['member'].size
print 'Unlimited dimension size =',member_max

if batch_mode:
    show_max = 0
else:
    # Limit the number of plots shown for an interactive job
    show_max = 2

# Get info about coordinate variables (read the data)
xsrc = fh.variables['xsrc'][:]
ysrc = fh.variables['ysrc'][:]
xsrc_bnds = fh.variables['xsrc_bnds'][:]
ysrc_bnds = fh.variables['ysrc_bnds'][:]
if verbose > 0:
    print "xsrc: ",xsrc
    print "ysrc: ",ysrc
    print "xsrc_bnds: ",xsrc_bnds
    print "ysrc_bnds: ",ysrc_bnds

xdst = fh.variables['xdst'][:]
ydst = fh.variables['ydst'][:]
xdst_bnds = fh.variables['xdst_bnds'][:]
ydst_bnds = fh.variables['ydst_bnds'][:]
if verbose > 0:
    print "xdst: ",xdst
    print "ydst: ",ydst
    print "xdst_bnds: ",xdst_bnds
    print "ydst_bnds: ",ydst_bnds

# Get info about the source data (do not read any data)
src_data_info = fh.variables['src_data']
if verbose > 0:
    print 'src_data dims  ',      src_data_info.dimensions
    print 'src_data shape ',      src_data_info.shape
    print "src_data long_name: ", src_data_info.long_name

# Get info about the destination data (do not read any data)
dst_data_info = fh.variables['dst_data']
if verbose > 0:
    print 'dst_data dims  ',      dst_data_info.dimensions
    print 'dst_data shape ',      dst_data_info.shape
    print "dst_data long_name: ", dst_data_info.long_name

# Get info about the interpolation error (do not read any data)
err1_data_info = fh.variables['err1_data']
if verbose > 0:
    print 'err1_data dims  ',      err1_data_info.dimensions
    print 'err1_data shape ',      err1_data_info.shape
    print "err1_data long_name: ", err1_data_info.long_name

# Get info about the re-interpolation error (do not read any data)
err2_data_info = fh.variables['err2_data']
if verbose > 0:
    print 'err2_data dims  ',      err2_data_info.dimensions
    print 'err2_data shape ',      err2_data_info.shape
    print "err2_data long_name: ", err2_data_info.long_name

# Get info about the source fraction (do not read any data)
src_frac_info = fh.variables['src_frac']
if verbose > 0:
    print 'src_frac dims  ',      src_frac_info.dimensions
    print 'src_frac shape ',      src_frac_info.shape
    print "src_frac long_name: ", src_frac_info.long_name

# Get info about the destination fraction (do not read any data)
dst_frac_info = fh.variables['dst_frac']
if verbose > 0:
    print 'dst_frac dims  ',      dst_frac_info.dimensions
    print 'dst_frac shape ',      dst_frac_info.shape
    print "dst_frac long_name: ", dst_frac_info.long_name

# Create an array containing the source data
src_data = src_data_info[:,:,:]

# Create an array containing the destination data
dst_data = dst_data_info[:,:,:]

# Create an array containing the interpolation error
err1_data = err1_data_info[:,:,:]

# Create an array containing the re-interpolation error
err2_data = err2_data_info[:,:,:]

# Create an array containing the source fraction
src_frac = src_frac_info[:,:,:]

# Create an array containing the destination fraction
dst_frac = dst_frac_info[:,:,:]

# Read src_area
src_area = fh.variables['src_area'][:,:]

# Read dst_area
dst_area = fh.variables['dst_area'][:,:]

# Read src_mask
src_mask = fh.variables['src_mask'][:,:,:]

# Read dst_mask
dst_mask = fh.variables['dst_mask'][:,:,:]

# Read the test descriptions
description = fh.variables['description'][:,:,:]

# Set the plot title
plot_title = "%s\n%s" % (nc_file_name, fh.test_function)

# Close the netcdf file
fh.close()

# Define arrays containing SICN on each of the source and destination grids
# Initialize SICN to zero for both grids
src_sicn = src_data[0,:,:]
if len(xsrc)==3 and len(ysrc)==3:
    src_sicn = np.array([[  0, 0.1,  -1],
                         [0.1, 0.1, 0.1],
                         [0.5, 0.5, 0.5]])
elif len(xsrc)==5 and len(ysrc)==5:
    src_sicn = np.array([[   0,  0, 1./15., 2./15., -1 ],
                         [ 1./12., 0, 1./8., 19./100., 1./10. ],
                         [ 3./22., 0, 1./10., 7./60., 1./10. ],
                         [   0, -1, 0.15, 87./140., 13./90. ], 
                         [ 0.5, 2./3., 0.5, 2./17., 13./18. ]])
elif len(xsrc)==6 and len(ysrc)==6:
    src_sicn = np.array([[  0,  0,  0,  0.1,  -1,  -1 ],
                         [  0,  0,  0,  0.3,  -1,  -1 ],
                         [0.3,  0,  0, 0.15, 0.1, 0.1 ],
                         [  0, -1, -1, 0.15, 0.1, 0.1 ],
                         [ -1, -1, -1,   -1,   1,   0 ],
                         [0.5, 0.5, 1,    0,   0,   1 ]])
else:
    src_sicn = 0

dst_sicn = dst_data[0,:,:]
if len(xdst)==3 and len(ydst)==3:
    dst_sicn = np.array([[  0, 0.1,  -1],
                         [0.1, 0.1, 0.1],
                         [0.5, 0.5, 0.5]])
elif len(xdst)==5 and len(ydst)==5:
    dst_sicn = np.array([[   0,  0, 1./15., 2./15., -1 ],
                         [ 1./12., 0, 1./8., 19./100., 1./10. ],
                         [ 3./22., 0, 1./10., 7./60., 1./10. ],
                         [   0, -1, 0.15, 87./140., 13./90. ], 
                         [ 0.5, 2./3., 0.5, 2./17., 13./18. ]])
elif len(xdst)==6 and len(ydst)==6:
    dst_sicn = np.array([[  0,  0,  0,  0.1,  -1,  -1 ],
                         [  0,  0,  0,  0.3,  -1,  -1 ],
                         [0.3,  0,  0, 0.15, 0.1, 0.1 ],
                         [  0, -1, -1, 0.15, 0.1, 0.1 ],
                         [ -1, -1, -1,   -1,   1,   0 ],
                         [0.5, 0.5, 1,    0,   0,   1 ]])
else:
    dst_sicn = 0

# src_frac is the fraction of each source cell participating in the remap
# dst_frac is the fraction of each destination cell participating in the remap
#
# Assume the src grid is the AGCM grid and the dst grid in the ocean (NEMO) grid
# and that masked values on the AGCM grid correspond to ocean and masked values
# on the NEMO grid correspond to land

###########################################################################
# Plot the data
###########################################################################

xmin_src = np.amin(xsrc_bnds)
xmax_src = np.amax(xsrc_bnds)
ymin_src = np.amin(ysrc_bnds)
ymax_src = np.amax(ysrc_bnds)

xmin_dst = np.amin(xdst_bnds)
xmax_dst = np.amax(xdst_bnds)
ymin_dst = np.amin(ydst_bnds)
ymax_dst = np.amax(ydst_bnds)

xmin = np.amin([xmin_src, xmin_dst])
xmax = np.amax([xmax_src, xmax_dst])
ymin = np.amin([ymin_src, ymin_dst])
ymax = np.amax([ymax_src, ymax_dst])

def draw_src_grid_lines(ax):
    ax.vlines(xsrc_bnds, ymin_src, ymax_src, colors='k', linestyles='solid', linewidths=1.5)
    ax.hlines(ysrc_bnds, xmin_src, xmax_src, colors='k', linestyles='solid', linewidths=1.5)

def draw_dst_grid_lines(ax):
    ax.vlines(xdst_bnds, ymin_dst, ymax_dst, colors='k', linestyles='solid', linewidths=1.5)
    ax.hlines(ydst_bnds, xmin_dst, xmax_dst, colors='k', linestyles='solid', linewidths=1.5)

def add_src_data_values(ax, dvals):
    # The transpose is required to reflect the order in which the data is rendered
    # ie matshow(... origin='lower', ...)
    dvals_tp = dvals.transpose()
    for ix in range(len(xsrc)):
        for iy in range(len(ysrc)):
            ax.text(xsrc[ix], ysrc[iy], "%.3g"%dvals_tp[ix,iy], \
                           horizontalalignment='center', verticalalignment='center', \
                           fontsize=10, alpha=1, color='black')

def add_dst_data_values(ax, dvals):
    # The transpose is required to reflect the order in which the data is rendered
    # ie matshow(... origin='lower', ...)
    dvals_tp = dvals.transpose()
    for ix in range(len(xdst)):
        for iy in range(len(ydst)):
            ax.text(xdst[ix], ydst[iy], "%.3g"%dvals_tp[ix,iy], \
                           horizontalalignment='center', verticalalignment='center', \
                           fontsize=10, alpha=1, color='black')

# This color map will be used for all figures
curr_cmap = "Pastel1_r"

# Use the input netcdf file name to define the output pdf file
fileout = re.sub('(?i)\.nc$','.pdf',nc_file_name)

# This with statement ensures that the PdfPages object is closed properly at
# the end of the block, even if an exception occurs
with PdfPages(fileout) as pdf:
    for page in range(2):
        member = 0
        for member in range(0,member_max):
            # Divide the figure into 6 subplots on a page with aspect ratio 8.5x11
            fig,axen = plt.subplots(3, 2, sharex=True, sharey=True, figsize=(8.5,11))

            ########################### Figure 0,0 ############################
            if page == 0:
                ##### Show source data
                axen[0,0].set_title('Source Data')
                src_vmin = src_data[member,:,:].min()
                src_vmax = src_data[member,:,:].max()
                img00 = axen[0,0].matshow(src_data[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=src_vmin, vmax=src_vmax, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'src_data: ',src_data[member,:,:]

                # Draw the src grid lines
                draw_src_grid_lines(axen[0,0])

                # Add data values as text
                add_src_data_values(axen[0,0], src_data[member,:,:])

            elif page == 1:
                # Show re-interpolation error
                axen[0,0].set_title('Re-Interpolation Error')
                err2_min = err2_data[member,:,:].min()
                err2_max = err2_data[member,:,:].max()
                img00 = axen[0,0].matshow(err2_data[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=err2_min, vmax=err2_max, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'err2_data: ',err2_data[member,:,:]

                # Draw the src grid lines
                draw_src_grid_lines(axen[0,0])

                # Add data values as text
                add_src_data_values(axen[0,0], err2_data[member,:,:])

            # hide all x tick labels for this sub plot
            plt.setp(axen[0,0].get_xticklabels(), visible=False)

            # Add a color bar for the source data
            divider = make_axes_locatable(axen[0,0])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img00, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)

            # print 'src_data: ',src_data[member,:,:]


            ########################### Figure 0,1 ############################
            if page == 0:
                ##### Show interpolated data
                axen[0,1].set_title('Interpolated Data')
                # Use the same min/max values found in the source data
                img01 = axen[0,1].matshow(dst_data[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=src_vmin, vmax=src_vmax, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'dst_data: ',dst_data[member,:,:]

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[0,1])

                # Add data values as text
                add_dst_data_values(axen[0,1], dst_data[member,:,:])

            elif page == 1:
                # Show interpolation error (compared with a known function)
                axen[0,1].set_title('Interpolation Error')
                # Use the same min/max values found in the err2 data
                img01 = axen[0,1].matshow(err1_data[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=err2_min, vmax=err2_max, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'err1_data: ',err1_data[member,:,:]

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[0,1])

                # Add data values as text
                add_dst_data_values(axen[0,1], err1_data[member,:,:])

            # hide all x tick labels for this sub plot
            plt.setp(axen[0,1].get_xticklabels(), visible=False)

            # Add a color bar for the destination data
            divider = make_axes_locatable(axen[0,1])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img01, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)


            ########################### Figure 1,0 ############################
            if page == 0:
                # Show source fraction
                axen[1,0].set_title('Source Fraction')
                img10 = axen[1,0].matshow(src_frac[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'src_frac: ',src_frac[member,:,:]

                # Draw the src grid lines
                draw_src_grid_lines(axen[1,0])

                # Add data values as text
                add_src_data_values(axen[1,0], src_frac[member,:,:])

            elif page == 1:
                # Show source mask
                axen[1,0].set_title('Source Mask')
                img10 = axen[1,0].matshow(src_mask[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'src_mask: ',src_mask[member,:,:]

                # Draw the src grid lines
                draw_src_grid_lines(axen[1,0])

                # Add data values as text
                add_src_data_values(axen[1,0], src_mask[member,:,:])

            # hide all x tick labels for this sub plot
            plt.setp(axen[1,0].get_xticklabels(), visible=False)

            # Add a color bar for the error data
            divider = make_axes_locatable(axen[1,0])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img10, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)


            ########################### Figure 1,1 ############################
            if page == 0:
                # Show destination fraction
                axen[1,1].set_title('Destination Fraction')
                img11 = axen[1,1].matshow(dst_frac[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'dst_frac: ',dst_frac[member,:,:]

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[1,1])

                # Add data values as text
                add_dst_data_values(axen[1,1], dst_frac[member,:,:])

            elif page == 1:
                # Show destination mask
                axen[1,1].set_title('Destination Mask')
                img11 = axen[1,1].matshow(dst_mask[member,:,:], origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'dst_mask: ',dst_mask[member,:,:]

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[1,1])

                # Add data values as text
                add_dst_data_values(axen[1,1], dst_mask[member,:,:])

            # hide x tick labels for this sub plot
            plt.setp(axen[1,1].get_xticklabels(), visible=False)

            # Add a color bar
            divider = make_axes_locatable(axen[1,1])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img11, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)


            ########################### Figure 2,0 ############################
            if page == 0:
                # Show source SICN
                axen[2,0].set_title('Source SICN')
                img20 = axen[2,0].matshow(src_sicn, origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'src_sicn: ',src_sicn

                # Draw the src grid lines
                draw_src_grid_lines(axen[2,0])

                # Add data values as text
                add_src_data_values(axen[2,0], src_sicn)

            elif page == 1:
                # Show terms in sum for source global average
                axen[2,0].set_title('Src Area*Frac*Value')
                # The source area of each grid cell that is required for the global average
                # calculation is the total area of that cell (src_area) multiplied by the
                # fraction of the cell participating in the remap (src_frac)
                src_avg  = src_area * src_frac[member,:,:] * src_data[member,:,:]
                img20 = axen[2,0].matshow(src_avg, origin='lower', cmap=curr_cmap, \
                                          extent=[xmin_src, xmax_src, ymin_src, ymax_src])
                # print 'src_avg: ',src_avg

                # Draw the src grid lines
                draw_src_grid_lines(axen[2,0])

                # Add data values as text
                add_src_data_values(axen[2,0], src_avg)


            # hide x tick labels for this sub plot
            plt.setp(axen[2,0].get_xticklabels(), visible=False)

            # Show x-axis tick labels on the bottom of the plot
            axen[2,0].tick_params(axis='x', labelbottom='on')

            # Add a color bar for the destination fraction
            divider = make_axes_locatable(axen[2,0])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img20, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)


            ########################### Figure 2,1 ############################
            if page == 0:
                # Show destination SICN
                axen[2,1].set_title('Destination SICN')
                img21 = axen[2,1].matshow(dst_sicn, origin='lower', cmap=curr_cmap, \
                                          vmin=0, vmax=1, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'dst_sicn: ',dst_sicn

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[2,1])

                # Add data values as text
                add_dst_data_values(axen[2,1], dst_sicn)

            elif page == 1:
                ###### Show destination global average
                axen[2,1].set_title('Dst Area*Frac*Value')
                # The destination area of each grid cell that is required for the global average
                # calculation is the total area of that cell (dst_area) multiplied by the fraction
                # of the cell participating in the remap (dst_frac)
                dst_avg  = dst_area * dst_frac[member,:,:] * dst_data[member,:,:]
                img21 = axen[2,1].matshow(dst_avg, origin='lower', cmap=curr_cmap, \
                                          extent=[xmin_dst, xmax_dst, ymin_dst, ymax_dst])
                # print 'dst_avg: ',dst_avg

                # Draw the destination grid lines
                draw_dst_grid_lines(axen[2,1])

                # Add data values as text
                add_dst_data_values(axen[2,1], dst_avg)

            # hide all x tick labels for this sub plot
            plt.setp(axen[2,1].get_xticklabels(), visible=False)

            # Show x-axis tick labels on the bottom of the plot
            axen[2,1].tick_params(axis='x', labelbottom='on')

            # Add a color bar for the destination fraction
            divider = make_axes_locatable(axen[2,1])
            cax = divider.append_axes("right", size="4%", pad=0.05)
            fig.colorbar(img21, cax=cax, orientation='vertical').ax.tick_params(labelsize=10)

            ##### Add a figure title
            # plot_title = 'Test %d from %s' % (member, nc_file_name)
            fig.suptitle(plot_title, fontsize=14)

            # hide x ticks for top plots and y ticks for right plots
            # plt.setp([a.get_xticklabels() for a in axen[0, :]], visible=False)
            # plt.setp([a.get_yticklabels() for a in axen[:, 1]], visible=False)

            # make subplots close to each other
            # fig.subplots_adjust(hspace=0.05)

            # Add test specific info to the page
            desc_method   = ''.join(description[member,0,:]).strip()
            desc_norm     = ''.join(description[member,1,:]).strip()
            desc_renorm   = ''.join(description[member,2,:]).strip()
            desc_src_gavg = ''.join(description[member,5,:]).strip()
            desc_dst_gavg = ''.join(description[member,6,:]).strip()

            lines = '%s\n%s\n%s' % (desc_method,desc_norm,desc_renorm)
            fig.text(0.05, 0.5, lines, horizontalalignment='left', \
                     verticalalignment='center', rotation='vertical')

            # Also add a note to the output pdf file
            pdf.attach_note(lines)

            # Calculate conserved global averages
            src_gavg  = src_area * src_frac[member,:,:] * src_data[member,:,:]
            dst_gavg  = dst_area * dst_frac[member,:,:] * dst_data[member,:,:]

            # Calculate specific global averages
            gavg_agcm3 = src_area * src_frac[member,:,:] * src_data[member,:,:] * (1-src_sicn)
            gavg_agcm4 = src_area * src_frac[member,:,:] * src_data[member,:,:] * src_sicn
            gavg_nemo3 = dst_area * dst_frac[member,:,:] * dst_data[member,:,:] * (1-dst_sicn)
            gavg_nemo4 = dst_area * dst_frac[member,:,:] * dst_data[member,:,:] * dst_sicn

            lines = r'$\Sigma_i F_{s,i} a_{s,i} f_{s,i} (1-SICN_{s,i}) = $' + '%.12g' % gavg_agcm3.sum()
            lines += '\n' + r'$\Sigma_i F_{d,i} a_{d,i} f_{d,i} (1-SICN_{d,i}) = $' + '%.12g' % gavg_nemo3.sum()
            fig.text(0.05,0.03,lines,horizontalalignment='left')

            lines = r'$\Sigma_i F_{s,i} a_{s,i} f_{s,i} SICN_{s,i} = $' + '%.12g' % gavg_agcm4.sum()
            lines += '\n' + r'$\Sigma_i F_{d,i} a_{d,i} f_{d,i} SICN_{d,i} = $' + '%.12g' % gavg_nemo4.sum()
            fig.text(0.45,0.03,lines,horizontalalignment='left')

            lines = r'$\Sigma_i F_{s,i} a_{s,i} f_{s,i} = $' + '%.12g\n' % src_gavg.sum()
            lines = lines + r'$\Sigma_i F_{d,i} a_{d,i} f_{d,i} = $' + '%.12g' % dst_gavg.sum()
            fig.text(0.75,0.03,lines,horizontalalignment='left')

            # Save the current figure as a page in the output pdf file
            pdf.savefig()

            # pages.append([fig])

            if member > show_max-1:
                # Disable displaying the figure at run time on the users terminal
                plt.close()
    
    # End of all plots
    plt.show()

print "Created",fileout
